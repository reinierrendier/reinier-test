#!/bin/sh

function md-launch {
    # Colors
    Y="\e[93m" # yellow
    R="\e[91m" # red
    G="\e[92m" # green
    M="\e[95m" # magenta
    C="\e[96m" # cyan
    U="\e[0m" # undo

    # input variables
    local_git=.
    release_name=""
    release_version=""
    service=""
    enviroment=""
    source="nexus"
    user=$USER
    local_path="false"

    # usage
    __usage="${Y}
        Usage: launch -r -s -v -e --help

        Options:
        -r, --release             name of the release
        -s, --service             name of the service
        -v, --version             version of the release
        -e, --env                 enviroment of release deployement
        -u, --user (additional)   change username
        -p, --path (additional)   path to local directory where release file is located, default is 'false'
                                  when default path is something other than 'false' it will set local as source
        -g, --git (additional)    path to local git repo, default is .

        Information:
        You add -SNAPSHOT to version to use a snapshot release file, however 
        nexus does not support this. 
       ${U}"

    for arg in "$@"
    do
        case $arg in
            -r|--release)
            release_name="$2"
            shift
            shift
            ;;
            -s|--service)
            service="$2"
            shift
            shift
            ;;
            -g|--git)
            local_git="$2"
            shift
            shift
            ;;
            -v|--version)
            release_version="$2"
            shift
            shift
            ;;
            -e|--env)
            enviroment="$2"
            shift
            shift
            ;;
            -u|--user)
            user="$2"
            shift
            shift
            ;;
            -p|--path)
            local_path="$2"
            shift
            shift
            ;;
            --help)
            echo -e "$__usage"
            return
            ;;          
        esac
    done

    # starting
    echo -e "${M}*** 1 2 3, script is launched! Enjoy the parade of passwords ***${U}"

    # Change source to local if path is specified
    if [ -d $local_path ]; then
        if [ $local_path != 'false' ]; then
            source="local"
        fi
    else
        echo -e "${R}Directory does not exist!${U}"
        break
    fi

    # get release file from nexus
    nexus_ip=172.20.24.181
    nexus_path=repository/maven-releases/tv/mediadistillery
    service_file=$service-$release_version
    release_file_path=http://$nexus_ip/$nexus_path/$release_name/$service/$release_version/$service_file
    if [ $source = 'nexus' ]; then
        echo -e "${C}Download $service_file from nexus${U}"
        nexus_get
    fi  
    if [ $source = 'local' ]; then
       echo -e "${Y}Using local directory as release file source${U}"
    fi

    # activate function populate_vm
    echo -e "${M}Configure Master${U}"
    populate_master 
 
    # activate function populate_vm
    echo -e "${M}Run puppet on agents${U}"
    puppet_agent 

    # complete
    echo -e "${M}Launch of release $release_version on $service-$enviroment is complete!${U}"
      
}

# get file from nexus
function nexus_get { 
    # url check
    file_type=""
    url_check_jar=`curl -Is $release_file_path.jar | head -n 1 | wc -c`
    url_check_war=`curl -Is $release_file_path.war | head -n 1 | wc -c`

    if [ $url_check_jar -eq 17 ]; then
        file_type=jar
    elif [ $url_check_war -eq 17 ]; then    
        file_type=war
    else
        echo -e "${R}Can't find release on nexus server, using break to cancel script${U}"
        break
    fi
    
    # add file_type to variables
    echo -e "${Y}file type is $file_type${U}"
    service_file=$service_file.$file_type
    release_file_path=$release_file_path.$file_type

    # download file from nexus
    wget -P . $release_file_path
    echo -e "${G}Download succeeded"

}

# put file on destinated host in /etc/puppet/files/services/
# Update git repo on master
function populate_master {

    # local file type check
    if [ $source = 'local' ]; then
        file_type=""
        if [ -f "$local_path/$service_file.jar" ]; then
            file_type=jar
        elif [ -f "$local_path/$service_file.war" ]; then
            file_type=war
        else 
            echo -e "${R}File type is not supported!${U}"
            echo -e "${R}Supported file types are: jar, war${U}"
            break
        fi
    # add file_type to variables
        echo -e "${Y}file type is $file_type${U}"
        service_file=$service_file.$file_type
    fi

    # variables
    puppet_master=puppet-$enviroment-001
    service_name=${service#"md-"} # remove md- in front of it
    service_name=${service_name#"-SNAPSHOT"} # remove -SNAPSHOT behind it
    service_name=${service_name//-} # remove hypens in the name
    puppet_path=/etc/puppet/files/$service_name
    git_puppet_path=/etc/puppet/md-hiera-$enviroment
    # change git puppet path if needed
    if [ $git_puppet_path = /etc/puppet/md-hiera-stg ]; then
        git_puppet_path="/etc/puppet/md-hiera-staging"
    fi
    if [ $git_puppet_path = /etc/puppet/md-hiera-prod ]; then
        git_puppet_path="/etc/puppet/md-hiera-production"
    fi
    if [ $git_puppet_path = /etc/puppet/md-hiera-dev ]; then
        git_puppet_path="/etc/puppet/md-hiera-development"
    fi

    # scp file to the master
    echo -e "${C}Connect to master, please${U}"
    scp $local_path/$service_file $user@$puppet_master:/home/$user
    echo -e "${G}scp succeeded"
    # clean up local enviroment
    if test -f "./$service_file" && [ $source = 'nexus' ]; then
        rm ./$service_file
        echo -e "${Y}./$service_file on local repository is removed"
    fi
    # update git files & move jar folder
    echo -e "${C}Connect to master, please${U}"
    ssh -t $user@$puppet_master "if [ -d $puppet_path ]; then
                                    echo move $service_file
                                    sudo mv /home/$user/$service_file $puppet_path && md5sum $puppet_path/$service_file
                                    echo update git repo $git_puppet_path
                                    sudo git -C $git_puppet_path pull
                                 else
                                    echo -e Directory not found!
                                 fi"
    if [ $source = 'nexus' ]; then
        echo -e "${Y}check md5sum by clicking on link $release_file_path.md5 "
    fi
}

# function to run puppet agent
function puppet_agent {
    # variables
    service_name=${service#"md-"} # remove md- in front of it
    service_name=${service_name#"-SNAPSHOT"} # remove -SNAPSHOT behind it
    service_name=${service_name//-} # remove hypens in the name
    puppet_agent=$service_name-$enviroment

    # check number of host in local git respority
    echo -e "${C}git update local repo${U}"
    git -C $local_git pull
    agent_number=`ls $local_git/node | grep $puppet_agent | wc -l`
    agent_number=`echo -e $agent_number | tr -d '[[:space:]]'`
    echo -e "${Y}There are $agent_number agent(s)"

    # run puppet agent command on agents
    if [ $agent_number -eq 0 ]; then
        echo -e "${R}Unable to find agents!${U}"
    else
        for i in {1..$agent_number}
            do echo -e "${C}Connect to $puppet_agent-00$i to run puppet${U}" 
            ssh -t $user@$puppet_agent-00$i "sudo puppet agent -t"
        done
        echo -e "${G}All $service_name agents are run on $enviroment"
    fi 
    
}



