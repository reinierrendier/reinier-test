#!/bin/sh


# input variables
release_name=still
release_version=1.69.0
service=md-overlay-detector
enviroment=stg-tel

# starting
echo "*** 1 2 3, script is launched! Enjoy the parade of passwords ***" 

# get release file from nexus
service_file=$service-$release_version
echo "Download $service_file from nexus"
# default variables
nexus_ip=172.20.24.181
nexus_path=repository/maven-releases/tv/mediadistillery
release_file_path=http://$nexus_ip/$nexus_path/$release_name/$service/$release_version/$service_file

# url check
file_type=""
url_check_jar=`curl -Is $release_file_path.jar | head -n 1 | wc -c`
url_check_war=`curl -Is $release_file_path.war | head -n 1 | wc -c`

if [ $url_check_jar -eq 17 ]; then
    file_type=jar
elif [ $url_check_war -eq 17 ]; then    
    file_type=war
else
    echo "Can't find release on nexus server."
    break
    return
fi

# add file_type to variables
echo "file type is $file_type"
service_file=$service_file.$file_type
release_file_path=$release_file_path.$file_type

# download file from nexus
wget -P $local_path $release_file_path
echo "Download succeeded"


# activate function populate_vm
echo "Configure Master"
# variables
service_name=${service#"md-"} # remove md- in front of it
service_name=${service_name//-} # remove hypens in the name
puppet_path=/etc/puppet/files/$service_name
git_path=/etc/puppet/md-hiera-$enviroment
puppet_master=puppet-$enviroment-001

# scp file to the master
echo "Connect to master, please"
scp $local_path/$service_file $USER@$puppet_master:/home/$USER
echo "scp succeeded" 
# clean up local enviroment
if test -f "$local_path/$service_file"; then
    rm $local_path/$service_file
    echo "$local_path/$service_file on local repository is removed" 
fi
# update git files
echo "Connect to master to update the git repository"
ssh -t $USER@$puppet_master "sudo git -C $git_path pull"
echo "updated md-hiera-$enviroment git repo" 

# move to right folder
echo "Connect to master to move the jar file on remote"
ssh -t $USER@$puppet_master "sudo mv /home/$USER/$service_file $puppet_path && md5sum $puppet_path/$service_file"
echo "$service_file is moved" 
echo "check md5sum by clicking on link $release_file_path.md5"


# activate function populate_vm
echo "Run puppet on agents"
# variables
service_name=${service#"md-"} # remove md- in front of it
service_name=${service_name//-} # remove hypens in the nam
service_git_repo=git@bitbucket.org:mediadistillery/md-hiera-$enviroment.git
puppet_agent=$service_name-$enviroment

# check number of host in local git respority
echo "git update local repo"
git checkout 
agent_number=`ls $local_git_path/node | grep $puppet_agent | wc -l`
agent_number=`echo -e $agent_number | tr -d '[[:space:]]'`
echo "There are $agent_number agent(s)"

# run puppet agent command on agents
for i in {1..$agent_number}
    do echo "Connect to $puppet_agent-00$i to run puppet" 
    ssh -t $USER@$puppet_agent-00$i "sudo puppet agent -t"
done    
echo "All $service_name agents are run on $enviroment" 

# complete
echo "Launch of release $release_version on $service-$enviroment is complete!" 
    


